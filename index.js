const FIRST_NAME = "Alina";
const LAST_NAME = "Matei";
const GRUPA = "1092";

function numberParser(a){
    if(isNaN(a) ||
        a >= Number.MAX_SAFE_INTEGER ||
        a <= Number.MIN_SAFE_INTEGER){
        return NaN;
    }
    else
        return Math.round(a);

}

function dynamicPropertyChecker(input, property) {

}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

console.log(numberParser(56.3));
console.log(numberParser(parseInt('56.3')));
console.log(numberParser(NaN));
console.log(numberParser(Infinity));
console.log(numberParser(-Infinity));
console.log(numberParser(Number.MAX_SAFE_INTEGER + 1));
console.log(numberParser(Number.MIN_SAFE_INTEGER - 1));